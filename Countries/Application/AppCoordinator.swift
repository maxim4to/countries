//
//  AppCoordinator.swift
//  Countries
//
//  Created by maxim.ivanov on 30/06/2019.
//  Copyright © 2019 maxim.ivanov. All rights reserved.
//

import Foundation
import UIKit

final class AppCoordinator {
    
    private weak var navigationController: UINavigationController?
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        showCountries()
    }
    
    private func showCountries() {
        let countriesCache = CountriesCache()
        let viewController = CountriesViewController()
        let viewModel = CountriesViewModel(countriesCache: countriesCache)
        _ = viewModel.countryTapObserver.bind { [weak self] country in
            self?.showCountryInfo(for: country, countriesCache: countriesCache)
        }
        viewController.configure(with: viewModel)
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    private func showCountryInfo(for country: Country, countriesCache: CountriesCache) {
        let viewController = CountryInfoViewController()
        let viewModel = CountryInfoViewModel(country: country, countriesCache: countriesCache)
        viewController.configure(with: viewModel)
        
        navigationController?.pushViewController(viewController, animated: true)
    }
}
