//
//  Array+SafeSubscript.swift
//  Countries
//
//  Created by maxim.ivanov on 29/06/2019.
//  Copyright © 2019 maxim.ivanov. All rights reserved.
//

import Foundation

extension Array {
    
    subscript(safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
