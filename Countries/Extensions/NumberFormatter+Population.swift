//
//  NumberFormatter+Population.swift
//  Countries
//
//  Created by maxim.ivanov on 30/06/2019.
//  Copyright © 2019 maxim.ivanov. All rights reserved.
//

import Foundation

extension NumberFormatter {
    
    static var populationFormatter: NumberFormatter {
        let numberFormatter = NumberFormatter()
        numberFormatter.usesGroupingSeparator = true
        numberFormatter.numberStyle = .decimal
        return numberFormatter
    }
}
