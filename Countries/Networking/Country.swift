//
//  Country.swift
//  Countries
//
//  Created by maxim.ivanov on 29/06/2019.
//  Copyright © 2019 maxim.ivanov. All rights reserved.
//

import Foundation

struct Country: Decodable {

    let name: String
    let population: Int
    let capital: String
    let borders: [String]
    let currencies: [Currency]
    let alpha3Code: String?
}
