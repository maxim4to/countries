//
//  CountriesService.swift
//  Countries
//
//  Created by maxim.ivanov on 29/06/2019.
//  Copyright © 2019 maxim.ivanov. All rights reserved.
//

import Foundation
import Moya
import Alamofire

enum CountriesService {
    case getCountries
}

extension CountriesService: TargetType {
    
    var baseURL: URL {
        return URL(string: "https://restcountries.eu")!
    }
    
    var path: String {
        switch self {
        case .getCountries:
            return "/rest/v2/all"
        }
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        return .requestPlain
    }
    
    var validationType: ValidationType {
        return .none
    }
    
    var headers: [String : String]? {
        return nil
    }
}
