//
//  CountriesCache.swift
//  Countries
//
//  Created by maxim.ivanov on 30/06/2019.
//  Copyright © 2019 maxim.ivanov. All rights reserved.
//

import Foundation

final class CountriesCache {
    
    var countries: [Country] = []
    
    func country(withCode code: String) -> Country? {
        return countries.first(where: { country -> Bool in
            country.alpha3Code == code
        })
    }
}
