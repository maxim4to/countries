//
//  Currency.swift
//  Countries
//
//  Created by maxim.ivanov on 29/06/2019.
//  Copyright © 2019 maxim.ivanov. All rights reserved.
//

import Foundation

struct Currency: Decodable {

    let code: String?
    let name: String?
    let symbol: String?
}
