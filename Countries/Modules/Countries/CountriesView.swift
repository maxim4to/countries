//
//  CountriesView.swift
//  Countries
//
//  Created by maxim.ivanov on 29/06/2019.
//  Copyright © 2019 maxim.ivanov. All rights reserved.
//

import UIKit
import Cartography

final class CountriesView: UIView {
    
    // MARK: - Subviews
    private(set) lazy var activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(style: .whiteLarge)
        indicator.color = .gray
        return indicator
    }()
    
    private(set) lazy var errorLabel: UILabel = {
        let label = UILabel()
        label.textColor = .gray
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    private(set) lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.delegate = nil
        tableView.dataSource = nil
        
        tableView.register(
            CountryTableViewCell.self,
            forCellReuseIdentifier: CountryTableViewCell.cellIdentifier
        )
        
        return tableView
    }()
    
    private(set) lazy var refreshControl = UIRefreshControl()
    
    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private
    private func configureUI() {
        tableView.refreshControl = refreshControl
        addSubview(tableView)
        constrain(tableView) {
            $0.edges == $0.superview!.edges
        }
        
        addSubview(activityIndicator)
        constrain(activityIndicator) {
            $0.center == $0.superview!.center
        }
        
        addSubview(errorLabel)
        constrain(errorLabel) {
            $0.leading == $0.superview!.leading + 40
            $0.top == $0.superview!.top + 40
            $0.trailing == $0.superview!.trailing - 40 ~ .init(999)
            $0.bottom == $0.superview!.bottom - 40 ~ .init(999)
        }
    }
}
