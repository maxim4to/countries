//
//  CountryTableViewCell.swift
//  Countries
//
//  Created by maxim.ivanov on 29/06/2019.
//  Copyright © 2019 maxim.ivanov. All rights reserved.
//

import UIKit
import Cartography

final class CountryTableViewCell: UITableViewCell {
    
    static let cellIdentifier = "CountryTableViewCell"
    
    // MARK: - Subviews
    private let countryNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 22, weight: .medium)
        label.numberOfLines = 2
        return label
    }()
    
    private let populationLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        return label
    }()
    
    // MARK: - Init
    init() {
        super.init(style: .default, reuseIdentifier: CountryTableViewCell.cellIdentifier)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Configuration
    func configure(with country: Country) {
        countryNameLabel.text = country.name
        let numberFormatter = NumberFormatter.populationFormatter
        let formattedNumber = numberFormatter.string(from: NSNumber(value: country.population))
        let population = formattedNumber ?? "\(country.population)"
        populationLabel.text = "Population: \(population)"
    }
    
    private func setupUI() {
        contentView.addSubview(countryNameLabel)
        constrain(countryNameLabel) {
            $0.top == $0.superview!.top + 12
            $0.leading == $0.superview!.leading + 20
            $0.trailing == $0.superview!.trailing - 20
        }
        
        contentView.addSubview(populationLabel)
        constrain(populationLabel, countryNameLabel) { populationLabel, countryNameLabel in
            populationLabel.top == countryNameLabel.bottom + 4
            populationLabel.leading == populationLabel.superview!.leading + 20
            populationLabel.trailing == populationLabel.superview!.trailing - 20
            populationLabel.bottom == populationLabel.superview!.bottom - 12
        }
        
    }
}
