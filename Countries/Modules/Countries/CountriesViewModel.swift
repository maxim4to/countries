//
//  CountriesViewModel.swift
//  Countries
//
//  Created by maxim.ivanov on 29/06/2019.
//  Copyright © 2019 maxim.ivanov. All rights reserved.
//

import Foundation
import Moya
import RxCocoa
import RxSwift

protocol CountriesViewModelProtocol: AnyObject {
    
    var contentState: Observable<CountriesViewModel.ContentState> { get }
    var pullToRefreshObserver: PublishSubject<Void> { get }
    var countryTapObserver: PublishSubject<Country> { get }
    
    func viewDidLoad()
}

final class CountriesViewModel: CountriesViewModelProtocol {
    
    enum ContentState {
        case initial
        case loading
        case data([Country])
        case error(String)
    }
    
    // MARK: - CountriesViewModelProtocol
    private(set) lazy var contentState = contentStateVariable.asObservable()
    private(set) lazy var pullToRefreshObserver = PublishSubject<Void>()
    private(set) lazy var countryTapObserver = PublishSubject<Country>()
    
    // MARK: - Private
    private let countriesCache: CountriesCache
    private let provider = MoyaProvider<CountriesService>(callbackQueue: DispatchQueue.global(qos: .utility))
    private let contentStateVariable = BehaviorRelay(value: CountriesViewModel.ContentState.initial)

    // MARK: - Init
    init(countriesCache: CountriesCache) {
        self.countriesCache = countriesCache
        
        _ = pullToRefreshObserver.subscribe { [weak self] _ in
            self?.loadCountries()
        }
    }
    
    // MARK: - RestaurantsViewModel
    func viewDidLoad() {
        loadCountries()
    }
    
    // MARK: - Private
    private func loadCountries() {
        contentStateVariable.accept(.loading)
        
        _ = provider.rx
            .request(.getCountries)
            .map([Country].self)
            .observeOn(MainScheduler())
            .subscribe { [weak self] event in
                switch event {
                case .success(let countries):
                    self?.contentStateVariable.accept(.data(countries))
                    self?.countriesCache.countries = countries
                    
                case .error(let error):
                    self?.contentStateVariable.accept(.error(error.localizedDescription))
                }
            }
    }
}
