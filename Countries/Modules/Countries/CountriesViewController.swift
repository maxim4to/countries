//
//  CountriesViewController.swift
//  Countries
//
//  Created by maxim.ivanov on 29/06/2019.
//  Copyright © 2019 maxim.ivanov. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

final class CountriesViewController: UIViewController {

    // MARK: - State
    private var disposeBag = DisposeBag()
    private var viewModel: CountriesViewModelProtocol?
    private var currentCountries = BehaviorRelay<[Country]>(value: [])
    
    // MARK: - Subviews
    private var tableView: UITableView? {
        return (view as? CountriesView)?.tableView
    }
    
    private var activityIndicator: UIActivityIndicatorView? {
        return (view as? CountriesView)?.activityIndicator
    }
    
    private var errorLabel: UILabel? {
        return (view as? CountriesView)?.errorLabel
    }
    
    private var refreshControl: UIRefreshControl? {
        return (view as? CountriesView)?.refreshControl
    }
    
    // MARK: - Init
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Configuration
    func configure(with viewModel: CountriesViewModelProtocol) {
        disposeBag = DisposeBag()
        self.viewModel = viewModel
        
        viewModel.contentState
            .bind { [weak self] state in
                self?.updateContent(for: state)
            }
            .disposed(by: disposeBag)

        refreshControl?.rx
            .controlEvent(.valueChanged)
            .subscribe(viewModel.pullToRefreshObserver)
            .disposed(by: disposeBag)
        
        guard let tableView = tableView else { return }

        tableView.rx
            .itemSelected.bind { [weak self] indexPath in
                self?.tableView?.deselectRow(at: indexPath, animated: true)
                
                guard let country = self?.currentCountries.value[safe: indexPath.row] else { return }
                viewModel.countryTapObserver.onNext(country)
            }
            .disposed(by: disposeBag)
        
        let cellIdentifier = CountryTableViewCell.cellIdentifier
        let cellType = CountryTableViewCell.self
        currentCountries
            .asDriver()
            .drive(
                tableView.rx.items(cellIdentifier: cellIdentifier, cellType: cellType),
                curriedArgument: { _, element, cell in
                    cell.configure(with: element)
                }
            )
            .disposed(by: disposeBag)
    }
    
    // MARK: - Lifecycle
    override func loadView() {
        view = CountriesView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel?.viewDidLoad()
    }

    // MARK: - Private
    private func updateContent(for state: CountriesViewModel.ContentState) {
        switch state {
        case .initial:
            break
            
        case .loading:
            currentCountries.accept([])
            if refreshControl?.isRefreshing == false {
                activityIndicator?.startAnimating()
            }
            errorLabel?.isHidden = true
            
        case .error(let description):
            currentCountries.accept([])
            activityIndicator?.stopAnimating()
            refreshControl?.endRefreshing()
            errorLabel?.isHidden = false
            errorLabel?.text = description
            
        case .data(let countries):
            currentCountries.accept(countries)
            activityIndicator?.stopAnimating()
            refreshControl?.endRefreshing()
            errorLabel?.isHidden = true
         }
    }
}

