//
//  CountryInfoViewController.swift
//  Countries
//
//  Created by maxim.ivanov on 29/06/2019.
//  Copyright © 2019 maxim.ivanov. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

final class CountryInfoViewController: UIViewController {
    
    // MARK: - State
    private var disposeBag = DisposeBag()
    private var viewModel: CountryInfoViewModelProtocol?
    
    private var countryNameLabel: UILabel? {
        return (view as? CountryInfoView)?.countryNameLabel
    }
    
    private var capitalNameLabel: UILabel? {
        return (view as? CountryInfoView)?.capitalNameLabel
    }
    
    private var populationLabel: UILabel? {
        return (view as? CountryInfoView)?.populationLabel
    }
    
    private var borderCountriesLabel: UILabel? {
        return (view as? CountryInfoView)?.borderCountriesLabel
    }
    
    private var currenciesLabel: UILabel? {
        return (view as? CountryInfoView)?.currenciesLabel
    }
    
    // MARK: - Lifecycle
    override func loadView() {
        view = CountryInfoView()
    }
    
    // MARK: - Configuration
    func configure(with viewModel: CountryInfoViewModelProtocol) {
        disposeBag = DisposeBag()
        self.viewModel = viewModel
        
        countryNameLabel.map {
            viewModel.countryNameTextObservable
                .bind(to: $0.rx.attributedText)
                .disposed(by:disposeBag)
        }
        
        capitalNameLabel.map {
            viewModel.capitalNameTextObservable
                .bind(to: $0.rx.attributedText)
                .disposed(by:disposeBag)
        }
        
        populationLabel.map {
            viewModel.populationTextObservable
                .bind(to: $0.rx.attributedText)
                .disposed(by:disposeBag)
        }
        
        currenciesLabel.map {
            viewModel.currenciesTextObservable
                .bind(to: $0.rx.attributedText)
                .disposed(by:disposeBag)
        }
        
        borderCountriesLabel.map {
            viewModel.bordersTextObservable
                .bind(to: $0.rx.attributedText)
                .disposed(by:disposeBag)
        }
    }
}
