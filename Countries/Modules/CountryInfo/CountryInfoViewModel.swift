//
//  CountryInfoViewModel.swift
//  Countries
//
//  Created by maxim.ivanov on 29/06/2019.
//  Copyright © 2019 maxim.ivanov. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

protocol CountryInfoViewModelProtocol {
    
    var countryNameTextObservable: Observable<NSAttributedString> { get }
    var capitalNameTextObservable: Observable<NSAttributedString> { get }
    var populationTextObservable: Observable<NSAttributedString> { get }
    var bordersTextObservable: Observable<NSAttributedString> { get }
    var currenciesTextObservable: Observable<NSAttributedString> { get }
}

final class CountryInfoViewModel: CountryInfoViewModelProtocol {
    
    // MARK: - Private
    private let country: Country
    private let countriesCache: CountriesCache
    
    // MARK: - CountryInfoViewModelProtocol
    let countryNameTextObservable: Observable<NSAttributedString>
    let capitalNameTextObservable: Observable<NSAttributedString>
    let populationTextObservable: Observable<NSAttributedString>
    let bordersTextObservable: Observable<NSAttributedString>
    let currenciesTextObservable: Observable<NSAttributedString>
    
    // MARK: - Init
    init(country: Country, countriesCache: CountriesCache) {
        self.country = country
        self.countriesCache = countriesCache
        
        let name = CountryInfoViewModel.attributedString(
            title: country.name,
            titleFont: UIFont.systemFont(ofSize: 32, weight: .semibold),
            description: "Country"
        )
        countryNameTextObservable = Observable.of(name)
        
        let capital = CountryInfoViewModel.attributedString(
            title: country.capital,
            titleFont: UIFont.systemFont(ofSize: 22, weight: .medium),
            description: "Capital"
        )
        capitalNameTextObservable = Observable.of(capital)
        
        let population = CountryInfoViewModel.populationString(population: country.population)
        populationTextObservable = Observable.of(population)
        
        let borders = CountryInfoViewModel.bordersString(
            borders: country.borders,
            countriesCache: countriesCache
        )
        bordersTextObservable = Observable.of(borders)
        
        let currencies = CountryInfoViewModel.currenciesString(currencies: country.currencies)
        currenciesTextObservable = Observable.of(currencies)
    }
    
    // MARK: - Private
    private static func populationString(population: Int) -> NSAttributedString {
        let numberFormatter = NumberFormatter.populationFormatter
        let formattedNumber = numberFormatter.string(from: NSNumber(value: population))
        return CountryInfoViewModel.attributedString(
            title: formattedNumber ?? "\(population)",
            titleFont: UIFont.systemFont(ofSize: 22, weight: .medium),
            description: "Population"
        )
    }
    
    private static func bordersString(borders: [String], countriesCache: CountriesCache) -> NSAttributedString {
        guard !borders.isEmpty else {
            return NSAttributedString()
        }
        
        let borders = borders.compactMap { countryCode -> NSAttributedString? in
            guard let country = countriesCache.country(withCode: countryCode) else { return nil }
            return NSAttributedString(
                string: country.name,
                attributes: mediumTextAttributes
            )
        }

        let titleLabel = NSAttributedString(
            string: "Borders:\n",
            attributes: smallTextAttributes
        )
        
        let resultString = NSMutableAttributedString()
        resultString.append(titleLabel)
        borders.enumerated().forEach { index, string in
            resultString.append(string)
            if index != borders.count - 1 {
                resultString.append(NSAttributedString(string: "\n"))
            }
        }
        
        return resultString
    }
    
    private static func currenciesString(currencies: [Currency]) -> NSAttributedString {
        guard !currencies.isEmpty else {
            return NSAttributedString()
        }
        
        let currencies = currencies.compactMap { currency -> NSAttributedString? in
            guard let name = currency.name, let code = currency.code else { return nil }
            return CountryInfoViewModel.attributedString(
                title: name,
                titleFont: UIFont.systemFont(ofSize: 20, weight: .regular),
                description: code
            )
        }

        let titleLabel = NSAttributedString(
            string: "Currencies:\n",
            attributes: smallTextAttributes
        )
        
        let resultString = NSMutableAttributedString()
        resultString.append(titleLabel)
        currencies.enumerated().forEach { index, string in
            resultString.append(string)
            if index != currencies.count - 1 {
                resultString.append(NSAttributedString(string: "\n"))
            }
        }
        
        return resultString
    }
    
    private static func attributedString(
        title: String,
        titleFont: UIFont,
        description: String)
        -> NSAttributedString
    {
        guard !title.isEmpty else { return NSAttributedString() }
        
        let attributedString = NSMutableAttributedString()
        attributedString.append(NSAttributedString(
            string: title,
            attributes: [
                NSAttributedString.Key.font: titleFont,
                NSAttributedString.Key.foregroundColor: UIColor.black
            ]
        ))
        attributedString.append(NSAttributedString(
            string: " / \(description)",
            attributes: smallTextAttributes
        ))
        return attributedString
    }
    
    private static let smallTextAttributes = [
        NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18, weight: .regular),
        NSAttributedString.Key.foregroundColor: UIColor.gray
    ]
    
    private static let mediumTextAttributes = [
        NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20, weight: .regular)
    ]
}
