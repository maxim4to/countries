//
//  CountryInfoView.swift
//  Countries
//
//  Created by maxim.ivanov on 29/06/2019.
//  Copyright © 2019 maxim.ivanov. All rights reserved.
//

import Foundation
import UIKit
import Cartography

final class CountryInfoView: UIView {
    
    private enum Spec {
        static let sideOffset: CGFloat = 20
    }
    
    let countryNameLabel = UILabel()
    let capitalNameLabel = UILabel()
    let populationLabel = UILabel()
    let borderCountriesLabel = UILabel()
    let currenciesLabel = UILabel()
    
    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private
    private func configureUI() {
        backgroundColor = .white
        
        let labels = [
            countryNameLabel,
            capitalNameLabel,
            populationLabel,
            borderCountriesLabel,
            currenciesLabel
        ]
        labels.forEach {
            $0.numberOfLines = 0
        }
        
        addSubview(countryNameLabel)
        constrain(countryNameLabel) {
            $0.top == $0.superview!.top + 24
            $0.leading == $0.superview!.leading + Spec.sideOffset
            $0.trailing == $0.superview!.trailing - Spec.sideOffset
        }
        
        addSubview(capitalNameLabel)
        constrain(capitalNameLabel, countryNameLabel) { capitalNameLabel, countryNameLabel in
            capitalNameLabel.top == countryNameLabel.bottom + 16
            capitalNameLabel.leading == capitalNameLabel.superview!.leading + Spec.sideOffset
            capitalNameLabel.trailing == capitalNameLabel.superview!.trailing - Spec.sideOffset
        }
        
        addSubview(populationLabel)
        constrain(populationLabel, capitalNameLabel) { populationLabel, capitalNameLabel in
            populationLabel.top == capitalNameLabel.bottom + 4
            populationLabel.leading == populationLabel.superview!.leading + Spec.sideOffset
            populationLabel.trailing == populationLabel.superview!.trailing - Spec.sideOffset
        }
        
        addSubview(borderCountriesLabel)
        constrain(borderCountriesLabel, populationLabel) { borderCountriesLabel, populationLabel in
            borderCountriesLabel.top == populationLabel.bottom + 16
            borderCountriesLabel.leading == borderCountriesLabel.superview!.leading + Spec.sideOffset
            borderCountriesLabel.trailing == borderCountriesLabel.superview!.trailing - Spec.sideOffset
        }
        
        addSubview(currenciesLabel)
        constrain(currenciesLabel, borderCountriesLabel) { currenciesLabel, borderCountriesLabel in
            currenciesLabel.top == borderCountriesLabel.bottom + 16
            currenciesLabel.leading == currenciesLabel.superview!.leading + Spec.sideOffset
            currenciesLabel.trailing == currenciesLabel.superview!.trailing - Spec.sideOffset
        }
    }
}
